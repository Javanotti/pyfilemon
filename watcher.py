import os
import sys
import time


class Watcher(object):
    running = True

    # File watching refresh rate (in seconds)
    refreshRate = 2

    # Constructor
    # @arg self : ....
    # @arg watchList : array of filenames for the files to observe
    # @arg actionOnChange : an optional, custom, function to call when a file is changed
    def __init__(self, watchList, *args, **kwargs):

        # Create an empty object to store the last-modified
        #  timestamp for each file in the watchlist
        self.timestampsCache = {}

        # The array of files to watch
        self.filenames = watchList

        self.args = args
        self.kwargs = kwargs

        # Flag to determine if first scan
        self.firstCheck = True

    # Custom action to invoke on file change event
    def onFileChange(self, fileName):
        print("Changed file: ", fileName)

    # Look for changes
    def fileChangeCheck(self):
        # Repeat for every file in the watchlist array
        for file in self.filenames:

            # Get the "last modified" timestamp
            stamp = os.stat(file).st_mtime

            # If the file ain't got a flag, flag it to 0
            if not file in self.timestampsCache:
                self.timestampsCache[file] = 0

            # If the timestamp is different from the one cached
            if stamp != self.timestampsCache[file]:
                self.timestampsCache[file] = stamp

                # File has changed, so do something...
                if self.firstCheck is False:
                    changedFile = open(file, 'r')
                    changedFile.seek(0)

                    self.onFileChange(file)

        if self.firstCheck is True:
            self.firstCheck = False

    # Keep watching in a loop
    def watch(self):
        while self.running:
            try:
                # Sleep for x seconds
                time.sleep(self.refreshRate)

                # Then watch for file changes
                self.fileChangeCheck()
            except KeyboardInterrupt:
                print('\nDone')
                break
            except FileNotFoundError:
                # Action on file not found
                pass
            except Exception as e:
                print(e)
                print('[Error]: %s' % sys.exc_info()[0])