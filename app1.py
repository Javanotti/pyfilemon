import json
import os
from watcher import Watcher


#watchList = ['./testfile1.txt', './testfile2.txt']
watchList = open("./watchlist.json", "r")
watchList = watchList.read()
watchList = json.loads(watchList)
watchList = watchList["files"]

if __name__ == "__main__":
    print("\n---- PyFileMon v0.1 ----")
    print("\nWatching for file changes....\n\n")

    watcher = Watcher(watchList) 
    watcher.watch()  # start the watch going